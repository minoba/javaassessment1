package nl.bioinf;

import java.util.Arrays;
import java.util.stream.IntStream;

public class PositionIntensities {
    private static final int NUCLEOTIDE_COUNT = 4;
    private static final char[] nucleotides = {'G', 'A', 'T', 'C'};
    private static final int SCORE_G_INDEX = 0;
    private static final int SCORE_A_INDEX = 1;
    private static final int SCORE_T_INDEX = 2;
    private static final int SCORE_C_INDEX = 3;

    private long position;
    private int[] scores = new int[NUCLEOTIDE_COUNT];

    public PositionIntensities(long position, int scoreG, int scoreA, int scoreT, int scoreC) {
        this.position = position;
        scores[SCORE_G_INDEX] = scoreG;
        scores[SCORE_A_INDEX] = scoreA;
        scores[SCORE_T_INDEX] = scoreT;
        scores[SCORE_C_INDEX] = scoreC;
    }

    /**
     * returns the Basecall of this position
     * @return
     */
    public Basecall getBasecall() {
        return null;
    }

    public long getPosition() {
        return position;
    }

    public int getScoreA() {
        return scores[SCORE_A_INDEX];
    }

    public int getScoreC() {
        return scores[SCORE_C_INDEX];
    }

    public int getScoreG() {
        return scores[SCORE_G_INDEX];
    }

    public int getScoreT() {
        return scores[SCORE_T_INDEX];
    }

    @Override
    public String toString() {
        return "PositionIntensities{" +
                "position=" + position +
                ", scoreA=" + getScoreA() +
                ", scoreC=" + getScoreC() +
                ", scoreG=" + getScoreG() +
                ", scoreT=" + getScoreT() +
                '}';
    }
}
