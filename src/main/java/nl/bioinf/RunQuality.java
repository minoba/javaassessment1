package nl.bioinf;

public class RunQuality {
    private double averageProbability;
    private int numberOfAmbiguities;

    public RunQuality(double averageProbability, int numberOfAmbiguities) {
        this.averageProbability = averageProbability;
        this.numberOfAmbiguities = numberOfAmbiguities;
    }

    public double getAverageProbability() {
        return averageProbability;
    }

    public int getNumberOfAmbiguities() {
        return numberOfAmbiguities;
    }

    @Override
    public String toString() {
        return "RunQuality{" +
                "averageProbability=" + averageProbability +
                ", numberOfAmbiguities=" + numberOfAmbiguities +
                '}';
    }
}
